package edu.put.inf151862_sn.Composable

import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.gestures.detectHorizontalDragGestures
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.input.pointer.pointerInput
import androidx.compose.ui.layout.onSizeChanged
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.font.FontFamily
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.navigation.NavController
import edu.put.inf151862_sn.Composable.Helpers.NavBar
import edu.put.inf151862_sn.AppTheme.SwitchDarkMode
import edu.put.inf151862_sn.Navigation.Screen
import edu.put.inf151862_sn.Navigation.StateManager
import edu.put.inf151862_sn.R
import edu.put.inf151862_sn.WindowInfo.RememberWindowInfo
import edu.put.inf151862_sn.WindowInfo.WindowInfo

@Composable
fun TabletMainPage(navController: NavController, stateManager: StateManager) {
    var windowInfo = RememberWindowInfo()
    if(windowInfo.screenWidthInfo is WindowInfo.WindowType.Phone){
        navController.navigate(Screen.PhoneMainScreen.route)
    }
    Column(
        modifier = Modifier.fillMaxSize().pointerInput(Unit) {
            detectHorizontalDragGestures(
                onHorizontalDrag = { change, dragAmount ->
                    when {
                        dragAmount < 0 -> {
                            navController.navigate(Screen.TabletListScreen.route)
                        }
                    }
                }
            )
        },
    ) {
        Column(
            modifier = Modifier.weight(1f).background(stateManager.brightBackgroundColor.value)
        ) {
            Row(modifier = Modifier
                .fillMaxWidth(1f)
                .padding(
                    top = 20.dp
                )
                .onSizeChanged {
                    stateManager.currentWidth.value = it
                },
                verticalAlignment = Alignment.CenterVertically,
                horizontalArrangement = Arrangement.SpaceAround){
                Box {

                }
                Text(
                    text = "Górski Stoper",
                    color = stateManager.fontColor.value,
                    modifier = Modifier,
                    fontSize = 60.sp,
                    fontFamily = FontFamily.Cursive
                )
                Box(modifier = Modifier
                    .padding(20.dp)){
                    SwitchDarkMode(stateManager)
                }
            }

            Image(
                painter = painterResource(id =  R.drawable.icon_mountain),
                contentDescription = "Image",
                modifier = Modifier
                    .size(250.dp)
                    .clip(CircleShape)
                    .align(Alignment.CenterHorizontally)
            )
        }
        NavBar(navController)
    }
}