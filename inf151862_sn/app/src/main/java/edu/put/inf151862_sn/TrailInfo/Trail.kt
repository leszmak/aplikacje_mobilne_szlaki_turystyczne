package edu.put.inf151862_sn.TrailInfo

import android.os.Parcelable
import androidx.annotation.DrawableRes

data class Trail(
    var id: Int,
    var name: String,
    var description: String,
    var time: Float,
    var difficulty: Int,
    var different: String,
    var pastTimes: Array<Long>,
    @DrawableRes var image: Int,
)

