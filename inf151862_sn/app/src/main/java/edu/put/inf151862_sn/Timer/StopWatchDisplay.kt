package edu.put.inf151862_sn.Timer

import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.width
import androidx.compose.material3.Button
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import edu.put.inf151862_sn.Navigation.StateManager

@Composable
fun StopWatchDisplay(stateManager: StateManager) {
    val SW = remember { StopWatch() }
    val savedTimes = remember { mutableStateOf(listOf<String>()) }

    Column(
        modifier = Modifier.fillMaxSize(),
        verticalArrangement = Arrangement.Center,
        horizontalAlignment = Alignment.CenterHorizontally
    ) {
        Text(
            text = SW.formattedTime,
            fontWeight = FontWeight.Bold,
            fontSize = 30.sp,
            color = stateManager.fontColor.value
        )
        Spacer(modifier = Modifier.height(16.dp))
        Row(
            horizontalArrangement = Arrangement.Center,
            verticalAlignment = Alignment.CenterVertically,
            modifier = Modifier.fillMaxWidth()
        ) {
            Button(onClick = {
                SW.start()  // Correctly call the start method
            }) {
                Text(text = "Start")
            }

            Spacer(modifier = Modifier.width(16.dp))
            Button(onClick = {
                SW.pause()  // Correctly call the pause method
            }) {
                Text(text = "Pause")
            }

            Spacer(modifier = Modifier.width(16.dp))
            Button(onClick = {
                SW.reset()  // Correctly call the reset method
            }) {
                Text(text = "Reset")
            }

            Spacer(modifier = Modifier.width(16.dp))
            Button(onClick = {
                savedTimes.value = savedTimes.value + SW.formattedTime  // Save the current time
            }) {
                Text(text = "Zapisz")
            }
        }

        Spacer(modifier = Modifier.height(16.dp))

        Column(
            verticalArrangement = Arrangement.Top,
            horizontalAlignment = Alignment.Start
        ) {
            savedTimes.value.forEach { time ->
                Text(
                    text = time,
                    fontSize = 20.sp,
                    color = stateManager.fontColor.value
                )
                Spacer(modifier = Modifier.height(4.dp))
            }
        }
    }
}
