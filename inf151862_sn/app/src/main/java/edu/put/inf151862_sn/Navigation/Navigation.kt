package edu.put.inf151862_sn.Navigation
import android.widget.Toast
import androidx.navigation.compose.rememberNavController

import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.saveable.rememberSaveable
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import androidx.navigation.compose.currentBackStackEntryAsState
import edu.put.inf151862_sn.Composable.DescriptionPage
import edu.put.inf151862_sn.Composable.KategoryPage
import edu.put.inf151862_sn.Composable.ListPage
import edu.put.inf151862_sn.Composable.MainPage

import androidx.compose.runtime.*
import androidx.lifecycle.viewmodel.compose.viewModel
import androidx.navigation.navArgument
import edu.put.inf151862_sn.Camera.Camera
import edu.put.inf151862_sn.Composable.TabletKategoryPage
import edu.put.inf151862_sn.Composable.TabletListPage
import edu.put.inf151862_sn.Composable.TabletMainPage
import edu.put.inf151862_sn.TrailInfo.TrailType
import edu.put.inf151862_sn.animation.AnimatedImageScreen

@Composable
fun Navigation() {
    var currentRoute by rememberSaveable { mutableStateOf(Screen.Animation.route) }
    val navController = rememberNavController()
    val stateManager = viewModel<StateManager>()

    NavHost(navController = navController, startDestination = currentRoute) {
        composable(route = Screen.Animation.route) {
            AnimatedImageScreen(navController = navController)
        }
        composable(route = Screen.PhoneMainScreen.route) {
            MainPage(navController = navController, stateManager = stateManager)
        }
        composable(route = Screen.PhoneListScreen.route) {
            ListPage(navController = navController, stateManager = stateManager)
        }
        composable(route = Screen.PhoneKategoryScreen.route) {
            KategoryPage(navController = navController, stateManager = stateManager)
        }
        composable( route = Screen.PhoneDescriptionScreen.route ) {
            DescriptionPage(navController = navController, stateManager = stateManager)
        }
        composable(route = Screen.TabletKategoryScreen.route) {
            TabletKategoryPage(navController = navController, stateManager = stateManager)
        }
        composable(route = Screen.TabletListScreen.route) {
            TabletListPage(navController = navController, stateManager = stateManager)
        }
        composable(route = Screen.TabletMainScreen.route) {
            TabletMainPage(navController = navController, stateManager = stateManager)
        }
        composable(route = Screen.Camera.route) {
            Camera(navController = navController)
        }
    }
}