package edu.put.inf151862_sn.WindowInfo

import androidx.compose.ui.unit.Dp

data class WindowInfo(
    val screenWidthInfo: WindowType,
    val screenHeightInfo: WindowType,
    val screenWidth: Dp,
    val screenHeight: Dp
) {
    sealed class WindowType{
        object Phone: WindowType()
        object Tablet: WindowType()
    }

}