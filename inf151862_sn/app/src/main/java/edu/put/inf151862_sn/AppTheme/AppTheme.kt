package edu.put.inf151862_sn.AppTheme

import androidx.compose.ui.graphics.Color

class AppTheme(
    var fontColor: Color,
    var brightBackgroundColor: Color,
    var darkBackgroundColor: Color,
    var mediumBackgroundColor: Color,
) {
    fun changeBackgroundToDark(){
        fontColor = Color.White
        brightBackgroundColor = Color.DarkGray
        darkBackgroundColor = Color.Black
        mediumBackgroundColor = Color.Gray
    }

    fun changeBackgroundToBright(){
        fontColor = Color.Black
        brightBackgroundColor = Color.White
        darkBackgroundColor = Color.Gray
        mediumBackgroundColor = Color.LightGray
    }

}