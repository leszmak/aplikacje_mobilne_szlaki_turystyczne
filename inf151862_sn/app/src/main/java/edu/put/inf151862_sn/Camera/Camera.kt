package edu.put.inf151862_sn.Camera

import android.widget.Toast
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxHeight
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.material3.Button
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalContext
import androidx.navigation.NavController
import edu.put.inf151862_sn.Navigation.Screen

@Composable
fun Camera(navController: NavController) {
    val context = LocalContext.current

    Column(
        modifier = Modifier.fillMaxSize(),
        horizontalAlignment = Alignment.CenterHorizontally
    ) {
        Box(modifier = Modifier) {
            Text(text = "Zrób zdjęcie!")
        }
        Spacer(modifier = Modifier.fillMaxHeight(0.8f))
        Row(
            modifier = Modifier.fillMaxWidth(),
            horizontalArrangement = Arrangement.Center,
            verticalAlignment = Alignment.CenterVertically
        ) {
            Button(onClick = {
                Toast.makeText(context, "Zrobiono zdjęcie!", Toast.LENGTH_SHORT).show()
            }) {
                Text(text = "Zrób zdjęcie")
            }
            Button(onClick = {
                navController.navigate(Screen.PhoneDescriptionScreen.route)
            }) {
                Text(text = "Powrót")
            }
        }
    }
}
