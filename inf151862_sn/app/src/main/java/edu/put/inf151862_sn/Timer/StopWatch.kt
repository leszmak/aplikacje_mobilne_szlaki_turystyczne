package edu.put.inf151862_sn.Timer

import androidx.compose.runtime.MutableState
import androidx.compose.runtime.*
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.cancel
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import java.time.Instant
import java.time.LocalDateTime
import java.time.ZoneId
import java.time.format.DateTimeFormatter
import java.util.Locale

class StopWatch(){
    var formattedTime by mutableStateOf("00:00")

    private var coroutineScope = CoroutineScope(Dispatchers.Main)
    private var isActive = false
    private var time = 0L
    private var lastTimestamp = 0L

    fun start(){
        if(isActive) return
        coroutineScope.launch {
            lastTimestamp = System.currentTimeMillis()/1000
            this@StopWatch.isActive = true
            while (this@StopWatch.isActive){
                delay(1000L)
                time += System.currentTimeMillis()/1000 - lastTimestamp
                lastTimestamp = System.currentTimeMillis()/1000
                formattedTime = formatTime(time)

            }
        }
    }

    fun pause(){
        isActive = false
    }

    fun reset(){
        coroutineScope.cancel()
        coroutineScope = CoroutineScope(Dispatchers.Main)
        time = 0L
        lastTimestamp = 0L
        formattedTime = "00:00"
        isActive = false
    }

    fun save(): String{
        var last = formattedTime
        coroutineScope.cancel()
        coroutineScope = CoroutineScope(Dispatchers.Main)
        time = 0L
        lastTimestamp = 0L
        formattedTime = "00:00"
        isActive = false
        return last
    }
    private fun formatTime(timeMillis: Long): String{
        val localDateTime = LocalDateTime.ofInstant(
            Instant.ofEpochSecond(timeMillis),
            ZoneId.systemDefault()
        )
        val formatter = DateTimeFormatter.ofPattern("mm:ss", Locale.getDefault())

        return localDateTime.format(formatter)
    }
}
