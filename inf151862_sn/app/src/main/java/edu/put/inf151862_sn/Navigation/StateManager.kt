package edu.put.inf151862_sn.Navigation

import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.unit.IntSize
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import edu.put.inf151862_sn.AppTheme.AppTheme
import edu.put.inf151862_sn.Timer.StopWatch
import edu.put.inf151862_sn.TrailInfo.TrailInfo
import kotlinx.coroutines.launch

class StateManager: ViewModel(){
    var currentTrail = mutableStateOf(TrailInfo().trails[0])
    var currentWidth = mutableStateOf(IntSize.Zero)
    var lastPage = mutableStateOf("")
    var fontColor = mutableStateOf(Color.Black)
    var brightBackgroundColor = mutableStateOf(Color.White)
    var darkBackgroundColor = mutableStateOf(Color.Gray)
    var mediumBackgroundColor = mutableStateOf(Color.LightGray)
    var darkModeButton by mutableStateOf(false)
}

