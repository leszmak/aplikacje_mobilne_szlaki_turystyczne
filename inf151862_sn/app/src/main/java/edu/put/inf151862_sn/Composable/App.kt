package edu.put.inf151862_sn.Composable

import androidx.compose.material3.BottomAppBar
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import edu.put.inf151862_sn.Composable.Helpers.KategoryList
import edu.put.inf151862_sn.Composable.Helpers.MyDescription
import edu.put.inf151862_sn.Navigation.Navigation

@Composable
fun App(modifier: Modifier = Modifier){
    Navigation()
}