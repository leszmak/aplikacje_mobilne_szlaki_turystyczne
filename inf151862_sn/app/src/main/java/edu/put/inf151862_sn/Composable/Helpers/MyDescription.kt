package edu.put.inf151862_sn.Composable.Helpers

import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxHeight
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.foundation.verticalScroll
import androidx.compose.material3.Button
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.font.FontFamily
import androidx.compose.ui.unit.IntSize
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.navigation.NavBackStackEntry
import androidx.navigation.NavController
import edu.put.inf151862_sn.Navigation.Screen
import edu.put.inf151862_sn.Navigation.StateManager
import edu.put.inf151862_sn.R
import edu.put.inf151862_sn.Timer.StopWatch
import edu.put.inf151862_sn.Timer.StopWatchDisplay
import edu.put.inf151862_sn.TrailInfo.Trail
import edu.put.inf151862_sn.TrailInfo.TrailType

@Composable
fun MyDescription(navController: NavController, stateManager: StateManager){

    var trail = stateManager.currentTrail.value
    println("aaaa MyDescription -> trail = stateManager.currentTrail.value: " + stateManager.currentTrail.value)

    Column(
        modifier = Modifier
            .fillMaxWidth(1f)
            .background(stateManager.brightBackgroundColor.value)
            .fillMaxHeight()
            .verticalScroll(rememberScrollState()),
            horizontalAlignment = Alignment.CenterHorizontally
    ) {

        Image(painter = painterResource(id =  trail.image), contentDescription = "Trail image", modifier = Modifier
            .size(250.dp)
            .clip(RoundedCornerShape(45.dp))
        )

        Text(
            text = "${trail.description}", Modifier
                .padding(20.dp),
            color = stateManager.fontColor.value,
            fontFamily = FontFamily.Monospace,
            fontSize = 15.sp
        )

        Text(
            text = "Czas potrzebny na przejście: ${trail.time} godziny", Modifier
                .align(Alignment.Start)
                .padding(20.dp),
            color = stateManager.fontColor.value,
            fontFamily = FontFamily.Monospace,
            fontSize = 15.sp
        )
        val stringBuilder = StringBuilder()
        for (number in 0..trail.difficulty) {
            stringBuilder.append("*")
        }
        Text(
            text = "Poziom trudności szlaku: ${stringBuilder}", Modifier
                .align(Alignment.Start)
                .padding(20.dp),
            color = stateManager.fontColor.value,
            fontFamily = FontFamily.Monospace,
            fontSize = 15.sp
        )

        StopWatchDisplay(stateManager)
        Button(modifier = Modifier.fillMaxWidth(),
            onClick = { navController.navigate(Screen.Camera.route) }) {
            Text(text = "Wykonaj zdjęcie")
        }



    }

}


