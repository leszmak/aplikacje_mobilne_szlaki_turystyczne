package edu.put.inf151862_sn.TrailInfo

import android.os.Bundle
import androidx.navigation.NavType
import com.google.gson.Gson

class TrailType : NavType<Trail>(isNullableAllowed = false) {
    override fun get(bundle: Bundle, key: String): Trail? {
        return bundle.getString(key)?.let{parseValue(it)}
    }
    override fun parseValue(value: String): Trail {
        return Gson().fromJson(value, Trail::class.java)
    }
    override fun put(bundle: Bundle, key: String, value: Trail) {
        bundle.putString(key, Gson().toJson(value))
    }
}