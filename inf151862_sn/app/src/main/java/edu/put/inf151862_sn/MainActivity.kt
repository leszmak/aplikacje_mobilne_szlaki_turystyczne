package edu.put.inf151862_sn

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.runtime.Composable
import androidx.compose.runtime.*
import androidx.compose.runtime.remember
import androidx.compose.ui.tooling.preview.Preview
import edu.put.inf151862_sn.Camera.Camera
import edu.put.inf151862_sn.Composable.App
import edu.put.inf151862_sn.Timer.StopWatch
import edu.put.inf151862_sn.Timer.StopWatchDisplay
import edu.put.inf151862_sn.animation.AnimatedImageScreen
import edu.put.inf151862_sn.ui.theme.Inf151862_snTheme

class MainActivity : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            Inf151862_snTheme {
                App()
            }
        }
    }
}

