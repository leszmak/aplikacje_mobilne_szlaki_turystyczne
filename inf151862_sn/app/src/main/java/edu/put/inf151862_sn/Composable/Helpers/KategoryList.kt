package edu.put.inf151862_sn.Composable.Helpers

import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxHeight
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.font.FontFamily
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.navigation.NavController
import edu.put.inf151862_sn.Navigation.Screen
import edu.put.inf151862_sn.Navigation.StateManager
import edu.put.inf151862_sn.TrailInfo.Trail
import edu.put.inf151862_sn.TrailInfo.TrailInfo


@Composable
fun KategoryList(navController: NavController, stateManager: StateManager, modifier: Modifier = Modifier
                     .fillMaxHeight(1f)
                     .fillMaxWidth(0.5f)
                     .background(stateManager.mediumBackgroundColor.value)
){

    val trails: List<Trail> = TrailInfo().trails
    LazyColumn(modifier = modifier
    ) {
        val sortedTrails = trails.sortedBy { it.difficulty }
        items(items = sortedTrails){ trail ->
            Row(modifier = Modifier
                .bottomBorder(1.dp, stateManager.fontColor.value)
                .fillMaxWidth()
                .clickable {
                    stateManager.currentTrail.value = trail
                    println("aaaa KategoryList -> stateManager.currentTrail.value: " + stateManager.currentTrail.value)
                    navController.navigate(Screen.PhoneDescriptionScreen.route)
                },
                verticalAlignment = Alignment.CenterVertically,
                horizontalArrangement = Arrangement.SpaceBetween,
            ){
                Image(painter = painterResource(id =  trail.image), contentDescription = "Image", modifier = Modifier
                    .size(150.dp)
                    .clip(RoundedCornerShape( 45.dp))
                    .padding(3.dp)
                )
                Text(
                    text = "${trail.name}",
                    modifier = Modifier
                        .padding(10.dp),
                    color = stateManager.fontColor.value,
                    fontFamily = FontFamily.Serif
                )

                val stringBuilder = StringBuilder()
                for (number in 0..trail.difficulty) {
                    stringBuilder.append("*")
                }
                Text(
                    text = "${stringBuilder}", Modifier
                        .padding(20.dp),
                    color = stateManager.fontColor.value,
                    fontFamily = FontFamily.Monospace,
                    fontSize = 15.sp
                )

            }
        }



    }
}