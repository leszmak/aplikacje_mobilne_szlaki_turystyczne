package edu.put.inf151862_sn.WindowInfo

import androidx.compose.runtime.Composable
import androidx.compose.ui.platform.LocalConfiguration
import androidx.compose.ui.unit.dp

@Composable

fun RememberWindowInfo(): WindowInfo{
    val configuration = LocalConfiguration.current
    return WindowInfo(
        screenWidthInfo = when {
            configuration.screenWidthDp < 600 -> WindowInfo.WindowType.Phone
            else  -> WindowInfo.WindowType.Tablet
        },
        screenHeightInfo = when {
            configuration.screenHeightDp < 480 -> WindowInfo.WindowType.Phone
            else  -> WindowInfo.WindowType.Tablet
        },
        screenWidth = configuration.screenWidthDp.dp,
        screenHeight =configuration.screenHeightDp.dp,
    )

}