package edu.put.inf151862_sn.Composable

import android.widget.Toast
import androidx.compose.foundation.background
import androidx.compose.foundation.gestures.detectHorizontalDragGestures
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxHeight
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.input.pointer.pointerInput
import androidx.compose.ui.layout.onSizeChanged
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.text.font.FontFamily
import androidx.compose.ui.unit.sp
import androidx.navigation.NavController
import edu.put.inf151862_sn.Composable.Helpers.KategoryList
import edu.put.inf151862_sn.Composable.Helpers.NavBar
import edu.put.inf151862_sn.AppTheme.SwitchDarkMode
import edu.put.inf151862_sn.Navigation.Screen
import edu.put.inf151862_sn.Navigation.StateManager
import edu.put.inf151862_sn.WindowInfo.RememberWindowInfo
import edu.put.inf151862_sn.WindowInfo.WindowInfo

@Composable
fun KategoryPage(navController: NavController, stateManager: StateManager){
    stateManager.lastPage.value = "kategory"
    var windowInfo = RememberWindowInfo()
    if(windowInfo.screenWidthInfo is WindowInfo.WindowType.Tablet){
        navController.navigate(Screen.TabletKategoryScreen.route)
    }
    Column(
        modifier = Modifier.fillMaxSize().pointerInput(Unit) {
            detectHorizontalDragGestures(
                onHorizontalDrag = { change, dragAmount ->
                    when {
                        dragAmount > 0 -> {
                            navController.navigate(Screen.PhoneListScreen.route)
                        }
                    }
                }
            )
        }
    ) {
        Column(
            modifier = Modifier.weight(1f),
            verticalArrangement = Arrangement.SpaceAround
        ) {
            Row(
                modifier = Modifier
                    .fillMaxWidth(1f)
                    .background(stateManager.darkBackgroundColor.value)
                    .onSizeChanged {
                        stateManager.currentWidth.value = it
                        println("aaaa KategoryPage -> stateManager.currentWidth.value: " + stateManager.currentWidth.value)
                    },
                verticalAlignment = Alignment.CenterVertically,
                horizontalArrangement = Arrangement.SpaceAround
            ) {
                Box {

                }
                Text(
                    text = "Trudność szlaków", Modifier,
                    color = stateManager.fontColor.value,
                    fontFamily = FontFamily.Monospace,
                    fontSize = 15.sp
                )
                SwitchDarkMode(stateManager)
            }

            KategoryList(navController, stateManager, Modifier
                    .fillMaxHeight(1f)
                    .fillMaxWidth(1f)
                    .background(stateManager.mediumBackgroundColor.value)
            )

        }
        NavBar(navController)
    }
}