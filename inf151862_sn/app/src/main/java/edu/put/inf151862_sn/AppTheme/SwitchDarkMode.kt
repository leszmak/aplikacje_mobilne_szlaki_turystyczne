package edu.put.inf151862_sn.AppTheme

import android.util.Log
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Switch
import androidx.compose.material3.SwitchDefaults
import androidx.compose.runtime.Composable
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.*
import androidx.compose.ui.graphics.Color
import edu.put.inf151862_sn.Navigation.StateManager



@Composable
fun SwitchDarkMode(stateManager: StateManager) {
    Switch(
        checked = stateManager.darkModeButton,
        onCheckedChange = {
            println("aaaa" + stateManager.darkModeButton)
            stateManager.darkModeButton = it
            if(stateManager.darkModeButton){
                stateManager.fontColor.value = Color.White
                stateManager.brightBackgroundColor.value = Color.Black
                stateManager.darkBackgroundColor.value = Color.DarkGray
                stateManager.mediumBackgroundColor.value = Color.Gray

            }
            else{
                stateManager.fontColor.value = Color.Black
                stateManager.brightBackgroundColor.value = Color.White
                stateManager.darkBackgroundColor.value = Color.Gray
                stateManager.mediumBackgroundColor.value = Color.LightGray
            }
        }
    )
}