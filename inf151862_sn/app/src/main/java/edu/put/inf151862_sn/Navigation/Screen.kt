package edu.put.inf151862_sn.Navigation

import edu.put.inf151862_sn.TrailInfo.Trail

sealed class Screen(val route: String) {
    object PhoneMainScreen: Screen("phoneMainScreen")
    object PhoneDescriptionScreen: Screen("phoneDescriptionScreen")
    object PhoneListScreen: Screen("phoneListScreen")
    object PhoneKategoryScreen: Screen("phoneKategoryScreen")
    object TabletKategoryScreen: Screen("tabletKategoryScreen")
    object TabletListScreen: Screen("tabletListScreen")
    object TabletMainScreen: Screen("tabletMainScreen")
    object Animation: Screen("animation")
    object Camera: Screen("camera")
}