package edu.put.inf151862_sn.TrailInfo

import edu.put.inf151862_sn.R

class TrailInfo {
    val trail1 = Trail(
        id = 1,
        name = "Beskid Śląski",
        description = "Skrzyczne jest trochę jak Kasprowy Wierch – też góruje nad popularnym kurortem, też jest rajem dla narciarzy, też można się tam dostać kolejką linową, też jest jednym z najpiękniejszych górskich szczytów, a widoki ze szczytu też zapierają dech w piersiach. Tyle że zamiast Tatr (które majaczą czasem na horyzoncie przy dobrej pogodzie) można podziwiać cały Beskid Śląski, Beskid Żywiecki, Jezioro Żywieckie oraz wiele innych malowniczych zakątków i ciekawych atrakcji turystycznych.",
        time = 3f,
        difficulty = 2,
        different = "Poziom trudności: **\n" +
                "Znaki: niebieskie lub zielone\n" +
                "Jak długo: ok. 2,5-3 godziny\n" +
                "Dlaczego warto: bo można popatrzeć nie tylko na okoliczne góry, ale też na jeziora i na paralotniarzy szybujących w dolinach",
        image = R.drawable.beskid_slaski,
        pastTimes = arrayOf()
    )
    val trail2 = Trail(
        id = 2,
        name = "Beskid Żywiecki",
        description = "Podobno powstała z kupy kamieni, którą pewna olbrzymka wysypała ongiś przed chałupą. Dlatego do dziś nazywa się Babią Górą. Ale są też tacy, co mówią na nią Diablak albo Kapryśnica. Bo nigdzie indziej w Polsce pogoda nie zmienia się równie szybko. A i diabelskich pułapek na nieostrożnych wędrowców nie brakuje. Zdobywając zaliczany do Korony Gór Polski najwyższy szczyt polskich Beskidów, trzeba więc mieć się na baczności – i nie zapomnieć o ubraniu przeciwdeszczowym. Kiedy już jednak pokonamy trasę z Przełęczy Krowiarki do schroniska na Markowych Szczawinach i następnie przez tzw. „Perć Akademików” na wierzchołek (klamry i łańcuchy w pakiecie), oczom naszym ukaże się zachwycająca panorama ponad 400 różnych szczytów. Kto nie wierzy, niech sam policzy.",
        time = 4.5f,
        difficulty = 4,
        different =  "Poziom trudności: ****\n" +
                "Znaki: niebieskie z Przełęczy Krowiarki do schroniska na Markowych Szczawinach (na przełęcz dojeżdżamy autem z Zawoi), następnie żółte na szczyt\n" +
                "Jak długo: ok. 4,5 godziny (plus podziwianie widoków i zejście)\n" +
                "Dlaczego warto: bo można tu poczuć prawdziwie wysokogórski klimat, nie będąc w Tatrach",
        image = R.drawable.beskid_zywiecki,
        pastTimes = arrayOf()
    )
    val trail3 = Trail(
        id = 3,
        name = "Góry Stołowe",
        description = "Małpa, wielbłąd, kura, słoń, a nawet mamut – to tylko część zwierzaków, jakie zobaczymy na szczycie Szczelińca Wielkiego. Niestety wskutek działań jakichś tajemniczych sił wszystkie zostały zamienione w kamień… Ale i tak robią wrażenie. Podobnie jak dziesiątki innych fantazyjnych form skalnych, które otaczają nas tu ze wszystkich stron. ",
        time = 3.5f,
        difficulty = 1,
        different = "Poziom trudności: *\n" +
                "Znaki: żółte\n" +
                "Jak długo: ok. 3,5 godziny w obie strony (jeśli nie zgubimy się w skalnym labiryncie)\n" +
                "Dlaczego warto: bo już Goethe zachwycał się tym miejscem, o czym dziś przypomina skromna tabliczka",
        image = R.drawable.gory_stolowe,
        pastTimes = arrayOf()
        )
    val trail4 = Trail(
        id = 4,
        name = "Góry Świętokrzyskie",
        description = "Najlepiej przypominać je sobie po kolei, przemierzając główny tutejszy szlak – trasa prowadzi z Nowej Słupi przez Święty Krzyż na Łysicę i dalej do Świętej Katarzyny. Miniemy po drodze kamienny posąg pielgrzyma, który co roku posuwa się do przodu o jedno ziarnko piasku, prastare miejsce kultu Słowian, którzy oddawali tu cześć bożkom zwanym Lelum i Polelum, imponujący klasztor na Świętym Krzyżu, a także skalne rumowiska, na których ponoć spotykały się okoliczne wiedźmy.",
        time = 6.5f,
        difficulty = 1,
        different = "Poziom trudności: *\n" +
                "Znaki: najpierw niebieskie, później czerwone\n" +
                "Jak długo: ok. 6-7 godzin\n" +
                "Dlaczego warto: bo w żadnych innych górach nie natkniemy się na tyle lokalnych legend i tajemniczych miejsc",
        image = R.drawable.gory_swietokrzyskie,
        pastTimes = arrayOf()
    )
    val trails: List<Trail> = listOf(trail1, trail2, trail3, trail4)
}