package edu.put.inf151862_sn.Composable

import android.widget.Toast
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.layout.onSizeChanged
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.text.font.FontFamily
import androidx.compose.ui.unit.sp
import androidx.navigation.NavController
import edu.put.inf151862_sn.Composable.Helpers.MyDescription
import edu.put.inf151862_sn.Composable.Helpers.NavBar
import edu.put.inf151862_sn.AppTheme.SwitchDarkMode
import edu.put.inf151862_sn.Navigation.Screen
import edu.put.inf151862_sn.Navigation.StateManager
import edu.put.inf151862_sn.Timer.StopWatch
import edu.put.inf151862_sn.Timer.StopWatchDisplay
import edu.put.inf151862_sn.WindowInfo.RememberWindowInfo
import edu.put.inf151862_sn.WindowInfo.WindowInfo

@Composable

fun DescriptionPage(navController: NavController, stateManager: StateManager){
    var windowInfo = RememberWindowInfo()
    //println("aaaa DescriptionPage -> windowInfo.screenWidthInfo: " + windowInfo.screenWidthInfo)

    if(windowInfo.screenWidthInfo is WindowInfo.WindowType.Tablet ){
        val context = LocalContext.current
        //println("aaaa DescriptionPage -> stateManager.lastPage.value: " + stateManager.lastPage.value)
        if(stateManager.lastPage.value == "list") {
            navController.navigate(Screen.TabletListScreen.route)
        }
        if(stateManager.lastPage.value == "kategory"){
            navController.navigate(Screen.TabletKategoryScreen.route)
        }
    }
    var trail = stateManager.currentTrail.value
    Column(
        modifier = Modifier.fillMaxSize()
    ) {
        Column(
            modifier = Modifier.weight(1f),
            verticalArrangement = Arrangement.SpaceAround
        ) {
            Row(
                modifier = Modifier
                    .fillMaxWidth(1f)
                    .background(stateManager.darkBackgroundColor.value)
                    .onSizeChanged {
                        stateManager.currentWidth.value = it
                    },
                verticalAlignment = Alignment.CenterVertically,
                horizontalArrangement = Arrangement.SpaceAround
            ) {
                Box {

                }
                Text(
                    text = "${trail.name}", Modifier,
                    color = stateManager.fontColor.value,
                    fontFamily = FontFamily.Monospace,
                    fontSize = 15.sp
                )
                SwitchDarkMode(stateManager)
            }
            MyDescription(navController, stateManager)

        }
        NavBar(navController)
    }
}