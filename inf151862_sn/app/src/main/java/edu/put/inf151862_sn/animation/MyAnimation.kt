package edu.put.inf151862_sn.animation

import androidx.compose.animation.AnimatedVisibility
import androidx.compose.animation.core.animateFloatAsState
import androidx.compose.animation.core.tween
import androidx.compose.animation.fadeIn
import androidx.compose.animation.fadeOut
import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.graphicsLayer
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.unit.dp
import androidx.navigation.NavController
import edu.put.inf151862_sn.Navigation.Screen
import edu.put.inf151862_sn.R
import kotlinx.coroutines.delay

@Composable
fun AnimatedImageScreen(navController: NavController) {
    var visible by remember { mutableStateOf(false) }
    val rotation by animateFloatAsState(
        targetValue = if (visible) 360f else 0f,
        animationSpec = tween(durationMillis = 3000)
    )

    LaunchedEffect(Unit) {
        delay(500) // Delay to simulate app starting
        visible = true

        delay(3000) // Duration for fadeIn and rotation
        visible = false // Start fade out

        delay(3000) // Duration for fadeOut
        // Perform any action after fade out is complete, if needed
        navController.navigate(Screen.PhoneMainScreen.route)
    }

    Box(
        modifier = Modifier.fillMaxSize(),
        contentAlignment = Alignment.Center
    ) {
        AnimatedVisibility(
            visible = visible,
            enter = fadeIn(animationSpec = tween(durationMillis = 3000)),
            exit = fadeOut(animationSpec = tween(durationMillis = 3000))
        ) {
            Image(
                painter = painterResource(id = R.drawable.icon_mountain),
                contentDescription = "Image",
                modifier = Modifier
                    .size(250.dp)
                    .clip(CircleShape)
                    .graphicsLayer(rotationZ = rotation)
            )
        }
    }
}