package edu.put.inf151862_sn.Photo
//
//import android.app.Activity
//import android.content.pm.PackageManager
//import android.graphics.Bitmap
//import android.widget.Toast
//import androidx.activity.compose.rememberLauncherForActivityResult
//import androidx.activity.result.contract.ActivityResultContracts
//import androidx.compose.foundation.Image
//import androidx.compose.foundation.layout.Box
//import androidx.compose.foundation.layout.fillMaxSize
//import androidx.compose.material.icons.Icons
//import androidx.compose.material3.FloatingActionButton
//import androidx.compose.material3.Icon
//import androidx.compose.material3.MaterialTheme
//import androidx.compose.material3.Scaffold
//import androidx.compose.runtime.Composable
//import androidx.compose.runtime.*
//import androidx.compose.ui.Modifier
//import androidx.compose.ui.graphics.asImageBitmap
//import androidx.compose.ui.platform.LocalContext
//import androidx.compose.ui.tooling.preview.Preview
//import androidx.core.app.ActivityCompat
//import androidx.core.content.ContextCompat
//import edu.put.inf151862_sn.Manifest
//
//@Composable
//fun Photo() {
//    val context = LocalContext.current
//    val bitmap = remember { mutableStateOf<Bitmap?>(null) }
//
//    val takePictureLauncher = rememberLauncherForActivityResult(
//        contract = ActivityResultContracts.TakePicturePreview()
//    ) { bmp: Bitmap? ->
//        if (bmp != null) {
//            bitmap.value = bmp
//            Toast.makeText(context, "Zdjęcie wykonane!", Toast.LENGTH_SHORT).show()
//        }
//    }
//
//    Scaffold(
//        floatingActionButton = {
//            FloatingActionButton(onClick = {
//                if (ContextCompat.checkSelfPermission(
//                        context,
//                        Manifest.permission.CAMERA
//                    ) == PackageManager.PERMISSION_GRANTED
//                ) {
//                    takePictureLauncher.launch(null)
//                } else {
//                    // Request permission
//                    ActivityCompat.requestPermissions(
//                        context as Activity,
//                        arrayOf(Manifest.permission.CAMERA),
//                        100
//                    )
//                }
//            }) {
//                Icon(imageVector = Icons.Default.CameraAlt, contentDescription = "Camera")
//            }
//        }
//    ) {
//        Box(modifier = Modifier.fillMaxSize()) {
//            bitmap.value?.let {
//                Image(bitmap = it.asImageBitmap(), contentDescription = null)
//            }
//        }
//    }
//}
//@Composable
//fun RequestCameraPermission(onPermissionResult: (Boolean) -> Unit) {
//    val context = LocalContext.current
//    val permissionLauncher = rememberLauncherForActivityResult(
//        contract = ActivityResultContracts.RequestPermission()
//    ) { isGranted: Boolean ->
//        onPermissionResult(isGranted)
//    }
//
//    LaunchedEffect(Unit) {
//        permissionLauncher.launch(Manifest.permission.CAMERA)
//    }
//}
//
//@Composable
//fun MyAppWithPermission() {
//    var hasPermission by remember { mutableStateOf(false) }
//    if (hasPermission) {
//        Photo()
//    } else {
//        RequestCameraPermission { isGranted ->
//            hasPermission = isGranted
//            if (!isGranted) {
//                Toast.makeText(LocalContext.current, "Permission denied", Toast.LENGTH_SHORT).show()
//            }
//        }
//    }
//}
//
//@Composable
//@Preview(showBackground = true)
//fun DefaultPreview() {
//    MaterialTheme {
//        MyAppWithPermission()
//    }
//}