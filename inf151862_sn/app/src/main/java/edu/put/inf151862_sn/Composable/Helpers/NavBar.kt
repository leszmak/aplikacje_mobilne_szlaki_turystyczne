package edu.put.inf151862_sn.Composable.Helpers

import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Home
import androidx.compose.material.icons.filled.Info
import androidx.compose.material.icons.filled.Star
import androidx.compose.material3.BottomAppBar
import androidx.compose.material3.Icon
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.navigation.NavController
import edu.put.inf151862_sn.Navigation.Screen


@Composable

fun NavBar(navController: NavController){
    BottomAppBar(modifier = Modifier
        .fillMaxWidth()
    ) {
        Row(
            modifier = Modifier.fillMaxWidth(),
            horizontalArrangement = Arrangement.SpaceAround
        ) {
            Icon(imageVector = Icons.Default.Home, contentDescription = "Home",
                modifier = Modifier.clickable{
                    navController.navigate(Screen.PhoneMainScreen.route)
                })
            Icon(imageVector = Icons.Default.Info, contentDescription = "Info",
                modifier = Modifier.clickable{
                    navController.navigate(Screen.PhoneListScreen.route)
                })
            Icon(imageVector = Icons.Default.Star, contentDescription = "Star",
                modifier = Modifier.clickable{
                    navController.navigate(Screen.PhoneKategoryScreen.route)
                })
        }
    }
}